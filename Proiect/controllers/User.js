
var utils = require('../utils/writer.js');
var User = require('../service/UserService');
var ObjectId = require('mongodb').ObjectID;

module.exports.addproject = function addproject (req, res, next) {
  var projectAsStr = req.swagger.params['project'].value.project;
  var project = null;
  try{
    //var decoded = Buffer.from(projectAsStr, 'base64').toString();
    project = JSON.parse(projectAsStr);
  }catch(e) {
    utils.writeJson(res, {"code" : 400, "description" : "Bad request"})
    return
  }
  if(req.session.user == null) {
    utils.writeJson(res, {"code" : 400, "description" : "Bad request - session expired"})
    return
  }
  User.addproject(project, req.session.user)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteproject = function deleteproject (req, res, next) {
  var projectId = req.swagger.params['projectId'].value;
  if(req.session.user == null) {
    utils.writeJson(res, {"code" : 400, "description" : "Bad request - session expired"})
    return
  }
  User.deleteproject(projectId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getworkspace = function getworkspace (req, res, next) {
  var public = req.swagger.params['public'].value;
  if(public == true){
    User.getworkspace(public, req.session.user)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
  }else{
    if(req.session.user == null) {
      utils.writeJson(res, {"code" : 400, "description" : "Bad request - session expired"})
      return
    }
    User.getworkspace(public, req.session.user)
      .then(function (response) {
        utils.writeJson(res, response);
      })
      .catch(function (response) {
        utils.writeJson(res, response);
      });
  }
};

module.exports.imagetoapi = function imagetoapi (req, res, next) {
  var params = JSON.parse(req.swagger.params['dataUri'].value)

  if(req.session.user == null) {
    utils.writeJson(res, {"code" : 400, "description" : "Bad request - session expired"})
    return
  }
  User.imagetoapi(params.content, params.isUrl)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.paragraphvaraints = function paragraphvaraints (req, res, next) {
  var text = req.swagger.params['text'].value;
  if(req.session.user == null) {
    utils.writeJson(res, {"code" : 400, "description" : "Bad request - session expired"})
    return
  }
  User.paragraphvaraints(text)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.readproject = function readproject (req, res, next) {
  var projectId = req.swagger.params['projectId'].value;
  // if(req.session.user == null) {
  //   utils.writeJson(res, {"code" : 400, "description" : "Bad request - session expired"})
  //   return
  // }
  User.readproject(projectId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.setprojectvisibility = function setprojectvisibility (req, res, next) {
  var projectId = req.swagger.params['projectId'].value;
  if(req.session.user == null) {
    utils.writeJson(res, {"code" : 400, "description" : "Bad request - session expired"})
    return
  }
  User.setprojectvisibility(projectId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.testtospeech = function testtospeech (req, res, next) {
  if(req.session.user == null) {
    utils.writeJson(res, {"code" : 400, "description" : "Bad request - session expired"})
    return
  }
  console.log(JSON.parse(req.swagger.params['text'].value));
  User.testtospeech({"email" : req.session.user.username, "title" : JSON.parse(req.swagger.params['text'].value)['title']})
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

main = function() {
  // User.addproject({
  //   "title": "breakig news",
  //   "isPublic": true
  // }, 'string').then(function(resp) {
  //   console.log(resp);
  // });

  // User.deleteproject(ObjectId('5ecce40971fc4d234d437936')).then(function(resp) {
  //   console.log(resp);
  // });

  // User.readproject(ObjectId("5ecce3eaa8c7872325d4f7dc")).then(function(resp) {
  //   console.log(resp);
  // });

  // User.setprojectvisibility(ObjectId("5ecce3eaa8c7872325d4f7dc")).then(function(resp) {
  //   console.log(resp);

  // User.getworkspace(true, 'string').then(function(resp) {
  //   console.log(resp);
  // });

  // User.imagetoapi('https://i.stack.imgur.com/vrkIj.png').then(function(resp) {
  //   console.log(resp);
  // });

  // User.addproject({
  //   '_id': ObjectId("5ecce3eaa8c7872325d4f7dc"),
  //   'title': 'iar am schimbat titlul',
  // }).then(function(resp) {
  //   console.log(resp);
  // });
}

// var fs = require('fs')
// var path = require('path')

// var contetn = fs.readFileSync(path.resolve(__dirname, '../service/ana.png'))
// console.log(typeof(contetn))
// User.imagetoapi(contetn, false).then(function (response) {console.log(response)})
