
var utils = require('../utils/writer.js');
var Guest = require('../service/GuestService');
const { BlobServiceClient } = require('@azure/storage-blob');
const AZURE_STORAGE_CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=cartiaudio;AccountKey=21gn/wjLgHjeZGy9yjtORHeFhqLDn+VhvFPrmMkfZgSvIjqBQpL5iaxd0dFQ3Hr7JWpVC/wsX4dPd7xyAjM75Q==;EndpointSuffix=core.windows.net";


async function createContainer(email) {
  console.log('start create container');
  // Create the BlobServiceClient object which will be used to create a container client
  const blobServiceClient = await BlobServiceClient.fromConnectionString(AZURE_STORAGE_CONNECTION_STRING);

  
  // Create a unique name for the container
  const containerName = email.replace('@', '').replace('.', '');


  console.log('\nCreating container...');
  console.log('\t', containerName);

  // Get a reference to a container
  const containerClient = await blobServiceClient.getContainerClient(containerName);

  // Create the container
  const createContainerResponse = await containerClient.create();
  console.log("Container was created successfully. requestId: ", createContainerResponse.requestId);
}

module.exports.authenticate = function authenticate (req, res, next) {
  var data = req.swagger.params['data'].value;

  //Perform signout
  if (data.email == "fake_signout") {
    req.session.user = null;
    utils.writeJson(res, "ok");
    return;
  }

  Guest.authenticate(data)
    .then(function (response) {
      if(response["code"] == 200) {
        req.session.user = {username : data["email"], id : response["description"]["id"]};
      }
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getworkspace = function getworkspace (req, res, next) {
  var public = req.swagger.params['public'].value;

  Guest.getworkspace(public)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.readproject = function readproject (req, res, next) {
  var projectId = req.swagger.params['projectId'].value;
  if(req.session.user == null){
    utils.writeJson(res, "BadRequest");
  }
  Guest.readproject(projectId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.register = function register (req, res, next) {
  var data = req.swagger.params['data'].value;
  Guest.register(data)
    .then(function (response) {
      if(response["code"] == 201) {
        createContainer(data["email"]).then(function () {
          req.session.user = {username : data["email"], id : response["description"]["id"]};
        });
          
      }
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};


Guest.getworkspace()
