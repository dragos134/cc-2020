var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var crypto = require('crypto');
var ObjectId = require('mongodb').ObjectID;
const azureFunctions = require('./azureFunctions')


// url-ul catre serverul tau mongo
var mongoUrl = 'mongodb://andi-mongo-cc:Vc1QFpJ7PyW78Ue32DCiw6VNCVTRbUqLJUimjouwwvhykF9rIWVFESAoLxBl2Ew6UIWphc6c3jy7OHlhnkdIPA%3D%3D@andi-mongo-cc.mongo.cosmos.azure.com:10255/?ssl=true&appName=@andi-mongo-cc@/?ssl=true';
var usersCollection = 'users';
var projectsCollection = 'projects';
var dbName = 'tema3_db';


/**
 * Register new user in dababase
 * Adds an item to the system
 *
 * data Data  (optional)
 * no response value expected for this operation
 **/
exports.register = function(data) {
  return new Promise(function(resolve, reject) {
    if(Object.keys(data).length != 5) {
      resolve({"code" : 400, "description" : "Invalid input"});
      return
    }

    if(!("email" in data) || !("password" in data) || !("passwordConfirmation" in data) ||
      !("firstName" in data) || !("lastName" in data)) {
      resolve({"code" : 400, "description" : "Invalid input"});
      return
    }
    
    if(data['password'] != data['passwordConfirmation']) {
      resolve({"code" : 410, "description" : "Passwords don't match"});
      return
    }
    var client = new MongoClient(mongoUrl, { "useUnifiedTopology": true });
    
    client.connect(function(err, client) {
      assert.equal(null, err);
      var db = client.db(dbName);
      var usersDB = db.collection(usersCollection);
      
      usersDB.findOne({"email" : data["email"]}).then(function (user) {
        if(user == null) {
          usersDB.insertOne({
            "email" : data["email"],
            "password" : crypto.Hash('sha256').update(data["password"]).digest('hex'),
            "firstName" : data["firstName"],
            "lastName" : data["lastName"]
          },  (err, result) => {
            if(err) reject({"status_eroare" : err})
            else resolve({"code" : 201, "description" : {"id" : result._id}})});
        } else {
          resolve({"code" : 409, "description" : "Existing user"});
        }

        client.close();
      });
    });
    
  
  });
}


/**
 * Authenticate user
 * Authenticate user
 *
 * data Data_1 Authentication data (optional)
 * returns inline_response_200
 **/
exports.authenticate = function(data) {
  return new Promise(function(resolve, reject) {
    if(Object.keys(data).length != 2) {
      resolve({"code" : "400", "description" : "Invalid input"});
      return
    }

    if(!("email" in data) || !("password" in data)) {
      resolve({"code" : "400", "description" : "Invalid input"});
      return
    }
    
    var client = new MongoClient(mongoUrl, { "useUnifiedTopology": true });
    
    client.connect(function(err, client) {
      assert.equal(null, err);
      var db = client.db(dbName);
      var usersDB = db.collection(usersCollection);
      
      usersDB.findOne({"email" : data["email"]}).then(function (user) {
        if(user == null) {
          resolve({"code" : "400", "description" : "User doesn't exist"});
        } else {
          if(crypto.Hash('sha256').update(data['password']).digest('hex') != user.password)
            resolve({"code" : "400", "description" : "Invalid password"});
          else {
            //req.session.user = {username : username, password: password}; de facut in router
            resolve({"code" : "200", "description" : {"id" : user._id}});
          }
        }

        client.close();
      });
    });
    
  
  });
}


/**
 * Get all public projects
 * Get all public projects
 *
 * returns List
 **/
exports.getworkspace = function() {
  return new Promise(function(resolve, reject) {
    var client = new MongoClient(mongoUrl, { "useUnifiedTopology": true });
    
    client.connect(function(err, client) {
      assert.equal(null, err);
      var db = client.db(dbName);
      var projectDB = db.collection(projectsCollection);
      

      projectDB.aggregate([
        {$match : {"isPublic" : true}},
        {
          $lookup : {
            from : usersCollection,
            localField : "ownerId",
            foreignField : "_id",
            as : "author"
          }
        },  
        {$project : 
          {"_id" : 1, "title" : 1, "createdDatetime" : 1, "author.email" : 1, "author.firstName" : 1, "author.lastName" : 1}
        }  
      ]).toArray(function (err, res) {
        if (err) reject({"err_mesage" : err})

        resolve({"code" : 200, "description" : res});
        
        client.close();
      });
    });
  });
}

/**
 * Get complete project value, including Content field
 * Get complete project value, including Content field
 *
 * projectId String The partially loaded project id, as known from previous /get_workspace response (optional)
 * returns Project
 **/
exports.readproject = function(projectId) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(mongoUrl, function(err, client) {
      var curr_db = client.db(dbName);
      var curr_col = curr_db.collection(projectsCollection);
      curr_col.findOne({
        '_id': ObjectId(projectId),
      }, function(err, obj) {
        if (err) {
          console.log('eroare la cautarea proiectului');
          resolve({'code': 400, 'description': 'eroare la cautarea proiectului'});
          throw err;
        }
        if (obj) {
          var other_col = curr_db.collection(usersCollection);
          other_col.findOne({
            '_id': obj['ownerId'],
          }, function(err, user) {
            obj['email'] = user['email'];
            resolve({'code': 200, 'description': obj});
          });
          
        }
        else {
          resolve({'code': 400, 'description': 'proiect de negasit'});
        }
      });
    });
  });
}

