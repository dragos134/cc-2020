const azureFunctions = require('./azureFunctions')
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
const url = 'mongodb://andi-mongo-cc:Vc1QFpJ7PyW78Ue32DCiw6VNCVTRbUqLJUimjouwwvhykF9rIWVFESAoLxBl2Ew6UIWphc6c3jy7OHlhnkdIPA%3D%3D@andi-mongo-cc.mongo.cosmos.azure.com:10255/?ssl=true&appName=@andi-mongo-cc@/?ssl=true';
const db = 'tema3_db';
const proj_col = 'projects';
const user_col = 'users';
const request = require('request');
const fs = require('fs');
const path = require('path')

// var session = require('express-session');

// const client = new MongoClient(url);
/**
 * Add new porject into database
 * Add new porject into database
 *
 * project Project The new project to be added or updated, if there is no existitg entry with project's id (optional)
 * returns Object
 **/
exports.addproject = function(project, user) {
  return new Promise(function(resolve, reject) {
    // if (!user || !("title" in project) || !("isPublic" in project)) {
    //   resolve({"code" : 400, "description" : "Bad Request"});
    //   return;
    // }
    MongoClient.connect(url, function(err, client) {
      if (err) {
        console.log('eroare la conectare');
        resolve({'code': 400, 'description': 'eroare neasteptata'});
        throw err;
      }
      var curr_db = client.db(db);
      var curr_col = curr_db.collection(proj_col);
      if (!project['_id'] || project['_id'] === "-1") {
        delete project._id;
        var other_col = curr_db.collection(user_col);
        other_col.findOne({'email': user.username}, function(err, resp) {
          if (err) {
            console.log('eroare la cautarea userului');
            resolve({'code': 400});
            throw err;
          }
          console.log(resp);
          project['ownerId'] = resp['_id'];
          project['createdDatetime'] = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
          project['content'] = '';
          curr_col.insertOne(project, function(err, inserted) {
            if(err) {
              console.log('eroare la inserare');
              throw err;
            }
            // console.log(inserted);
            resolve({"code" : 200, "description" : {"newProjectAdded" : true}});
          });
        });
      }
      else {
        var setProject = {
          content: project.content,
          title: project.title,
          isPublic: project.isPublic
        };
        curr_col.updateOne({
          '_id': ObjectId(project['_id']),
        }, {$set : setProject}, function(err, response) {
          if (err) {
            console.log('eroare la update!');
            resolve({'code': 400, 'description': 'eroare neasteptata'});
            throw err;
          }
          resolve({'code': 200, 'description': `uploaded ${response}`});
        });
      }
    });
  });
}


/**
 * Delete project by id
 * Delete project by id
 *
 * projectId String Project's id (optional)
 * no response value expected for this operation
 **/
exports.deleteproject = function(projectId) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, function(err, client) {
      if (err) {
        console.log('eroare la conectare cu mongo');
        throw err;
      }
      var curr_db = client.db(db);
      var curr_col = curr_db.collection(proj_col);
      curr_col.deleteOne({
        "_id": ObjectId(projectId),
      }, function(err, obj) {
        if (err) {
          resolve({"code": 400, "description": "error on deletion"});
          throw err;
        }
        if (obj['deletedCount'] == 1) {
          resolve({"code": 200, "description":`${projectId} removed`});
          return
        }
        else {
          resolve({"code": 400, "description": "object not found"});
          return
        }
      });

    });
  });
}


/**
 * Get all projects in certain workspace
 * Get all projects in certain workspace
 *
 * public Boolean If public is true, then returns all public workspace items, else will return only current user's projects (optional)
 * returns List
 **/
exports.getworkspace = function(public, user) {
  return new Promise(function(resolve, reject) {
    if (public == true) {
      MongoClient.connect(url, function(err, client) {
        var curr_db = client.db(db);
        var curr_col = curr_db.collection(proj_col);

        curr_col.aggregate([
          {$match : {"isPublic" : true}},
          {
            $lookup : {
              from : user_col,
              localField : "ownerId",
              foreignField : "_id",
              as : "author"
            }
          },  
          {$project : 
            {"_id" : 1, "title" : 1, "createdDatetime" : 1, "author.email" : 1, "author.firstName" : 1, "author.lastName" : 1}
          }  
        ]).toArray(function (err, res) {
          if (err) reject({"err_mesage" : err})
  
          resolve({"code" : 200, "description" : res});
          
          client.close();
        });
      });
    }
    else {
      MongoClient.connect(url, function(err, client) {
        var curr_db = client.db(db);
        var curr_col = curr_db.collection(proj_col);
        var other_col = curr_db.collection(user_col);
        other_col.findOne({
          'email': user.username,
        }, function(err, resp) {
          if (err) {
            throw err;
          }
          curr_col.find({
            'ownerId': resp['_id'],
          }).toArray(function(err, result){
            if (err) {
              throw err;
            }
            resolve(result);
          });
        });
      });

    }
  });
}


/**
 * AZURE-API
 * Send image and receive text, the request is performed via azure services
 * Send image and receive text, the request is performed via azure services
 *
 * dataUri String Base64 encoded image (optional)
 * returns Object
 **/
exports.imagetoapi = function(content, isUrl) {
  return new Promise(function(resolve, reject) {
    let subscriptionKey = "ccd0eb9d3a3e46f385585c7d0e0a24f1";
    let endpoint = "https://computer-vision-ccc.cognitiveservices.azure.com/";
    if (!subscriptionKey) { throw new Error('Set your environment variables for your subscription key and endpoint.'); }

    var uriBase = endpoint + 'vision/v2.1/ocr';

    var imageUrl = content;

    if(isUrl) {
      try{
        content = Buffer.from(imageUrl, 'base64');
      }catch(e){
        console.log("error: " + e);
      }
      isUrl = false;
    }

    // Request parameters.
    const params = {
        'language': 'unk',
        'detectOrientation': 'true',
    };

    const options = {
        uri: uriBase,
        qs: params,
        body: isUrl ? `{"url": "${imageUrl}"}` : content, //``,
        headers: {
            'Content-Type': isUrl ? 'application/json' : 'application/octet-stream',
            'Ocp-Apim-Subscription-Key' : subscriptionKey
        }
    };
    console.log("OPTIONS")
    console.log(options)

    request.post(options, (error, response, body) => {
      if (error) {
        console.log('Error: ', error);
        resolve({'code': 401, 'description': {'error': error}});
        return;
      }
      console.log(body);
      var finalResult = "";
      let jsonResponse = JSON.parse(body);
      for (var region in jsonResponse["regions"]) {
        for (var line in jsonResponse["regions"][region]["lines"]) {
          for (var word in jsonResponse["regions"][region]["lines"][line]["words"]) {
            finalResult = finalResult + ' ' + jsonResponse["regions"][region]["lines"][line]["words"][word]["text"];
          }
          finalResult = finalResult + '\n';
        }
      }

      resolve({'code': 200, 'description': {'text': finalResult}});
    });
  });
}


/**
 * (Optional) Send paragraph and receive possible continue variants. Azure API is used
 * (Optional) Send paragraph and receive possible continue variants. Azure API is used
 *
 * text String Complete paraghraph to get ideas for continuation (optional)
 * returns List
 **/
exports.paragraphvaraints = function(text) {
  return new Promise(async function(resolve, reject) {
    azureFunctions.spellCheck(text, function(response) {
      resolve({"code" : 200, "description" : {"checked_text" : response }})
    })
    
  });
}


/**
 * Get complete project value, including Content field
 * Get complete project value, including Content field
 *
 * projectId String The partially loaded project id, as known from previous /get_workspace response (optional)
 * returns Project
 **/
exports.readproject = function(projectId) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, function(err, client) {
      var curr_db = client.db(db);
      var curr_col = curr_db.collection(proj_col);
      curr_col.findOne({
        '_id': ObjectId(projectId),
      }, function(err, obj) {
        if (err) {
          console.log('eroare la cautarea proiectului');
          resolve({'code': 400, 'description': 'eroare la cautarea proiectului'});
          throw err;
        }
        if (obj) {
          var other_col = curr_db.collection(user_col);
          other_col.findOne({
            '_id': obj['ownerId'],
          }, function(err, user) {
            obj['email'] = user['email'];
            resolve({'code': 200, 'description': obj});
          });
          
        }
        else {
          resolve({'code': 400, 'description': 'proiect de negasit'});
        }
      });
    });
  });
}


/**
 * Set project's visibility
 * Set project's visibility
 *
 * projectId String Project's id (optional)
 * no response value expected for this operation
 **/
exports.setprojectvisibility = function(projectId) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, function(err, client) {
      var curr_db = client.db(db);
      var curr_col = curr_db.collection(proj_col);
      curr_col.findOne({
        '_id': projectId,
      }, function(err, obj) {
        if (err) {
          resolve({"code": 400, "description": "project not found"});
        }
        if(obj) {
          obj['isPublic'] = !obj['isPublic'];
          curr_col.updateOne({
            '_id': obj['_id'],
          }, { $set: {'isPublic': obj['isPublic']}});
          resolve({"code": 200, "description": "visibility changed"});
        }
        else{
          resolve({'code': 400, 'description': 'proiect de negasit'});
        }
        
      });
    });
  });
}

/**
 * AZURE-API
 * Send text and receive mp3 stream. Processing is done via
 * Send text and receive mp3 stream. Processing is done via Azure API
 *
 * data - contains
 * email String - User's email
 * title String - Project's title
 * returns Object
 **/

function prelucrareContent(content) {
  if(!("blocks" in content)) return ""
  let blocks = content.blocks
  var sir = ""
  for(var i = 0; i < blocks.length; i++) {
    sir += blocks[i].data.text
  }

  return sir.replace('/\n', '')
}
exports.testtospeech = function(data) {
  return new Promise(async function(resolve, reject) {
    if(Object.keys(data).length != 2) {
      resolve({"code" : 400, "description" : "Bad request" });
      return
    }
    if(!("email" in data) || !("title" in data)) {
      resolve({"code" : 400, "description" : "Bad request" });
      return
    }
    //console.log(data);
    let username = data["email"].replace('@', '').replace('.', '')

    var client = new MongoClient(url, { "useUnifiedTopology": true });
    
    client.connect(function(err, client) {
      // assert.equal(null, err);
      var my_db = client.db(db);
      var projectsDB = my_db.collection(proj_col);
      
      projectsDB.findOne({"title" : data["title"]}, function(err, project) {
        let text = prelucrareContent(JSON.parse(project.content))
        //console.log('proiectul este ' + JSON.stringify(project));

        azureFunctions.textToSpeech(text, username, data["title"]+".mp3", function() {
          var music_url = azureFunctions.generateSASurl(username, data["title"] + ".mp3");
          resolve({"code": 201, "description" : music_url})//{"mp3Url": azureFunctions.generateSASurl(username, data["title"]+".mp3")}})
          client.close();
        });
      });
        
      });
    });
    
    
    
}

