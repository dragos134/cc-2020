
const { BlobServiceClient } = require('@azure/storage-blob');
var blobStorage = require('@azure/storage-blob');
let https = require ('https');

var speechSdk = require("microsoft-cognitiveservices-speech-sdk");
const { TextAnalyticsClient, AzureKeyCredential } = require("@azure/ai-text-analytics");

var languageKey = "754f58a28b804d1d962f5117e7347bdb";
var languageEndpoint = "https://text-analytics-cc.cognitiveservices.azure.com/text/analytics/v2.1/languages";
const textAnalyticsClient = new TextAnalyticsClient(languageEndpoint,  new AzureKeyCredential(languageKey));

function getLocale(countryCode) {
    // returneaza locale-ul, dat fiind codul tarii
    var locales = ['af-ZA',
                    'am-ET',
                    'ar-EG',
                    'arn-CL',
                    'as-IN',
                    'az-Cyrl-AZ',
                    'az-Latn-AZ',
                    'ba-RU',
                    'be-BY',
                    'bg-BG',
                    'bn-BD',
                    'bn-IN',
                    'bo-CN',
                    'br-FR',
                    'bs-Cyrl-BA',
                    'bs-Latn-BA',
                    'ca-ES',
                    'co-FR',
                    'cs-CZ',
                    'cy-GB',
                    'da-DK',
                    'de-DE',
                    'dsb-DE',
                    'dv-MV',
                    'el-GR',
                    'en-US',
                    'es-ES',
                    'et-EE',
                    'eu-ES',
                    'fa-IR',
                    'fi-FI',
                    'fil-PH',
                    'fo-FO',
                    'fr-FR',
                    'fy-NL',
                    'ga-IE',
                    'gd-GB',
                    'gl-ES',
                    'gsw-FR',
                    'gu-IN',
                    'ha-Latn-NG',
                    'he-IL',
                    'hi-IN',
                    'hr-HR',
                    'hsb-DE',
                    'hu-HU',
                    'hy-AM',
                    'id-ID',
                    'ig-NG',
                    'ii-CN',
                    'is-IS',
                    'it-IT',
                    'iu-Cans-CA',
                    'iu-Latn-CA',
                    'ja-JP',
                    'ka-GE',
                    'kk-KZ',
                    'kl-GL',
                    'km-KH',
                    'kn-IN',
                    'kok-IN',
                    'ko-KR',
                    'ky-KG',
                    'lb-LU',
                    'lo-LA',
                    'lt-LT',
                    'lv-LV',
                    'mi-NZ',
                    'mk-MK',
                    'ml-IN',
                    'mn-MN',
                    'mn-Mong-CN',
                    'moh-CA',
                    'mr-IN',
                    'ms-BN',
                    'ms-MY',
                    'mt-MT',
                    'nb-NO',
                    'ne-NP',
                    'nl-BE',
                    'nl-NL',
                    'nn-NO',
                    'nso-ZA',
                    'oc-FR',
                    'or-IN',
                    'pa-IN',
                    'pl-PL',
                    'prs-AF',
                    'ps-AF',
                    'pt-BR',
                    'pt-PT',
                    'qut-GT',
                    'quz-BO',
                    'quz-EC',
                    'quz-PE',
                    'rm-CH',
                    'ro-RO',
                    'ru-RU',
                    'rw-RW',
                    'sah-RU',
                    'sa-IN',
                    'se-FI',
                    'se-SE',
                    'si-LK',
                    'sk-SK',
                    'sl-SI',
                    'sma-NO',
                    'sma-SE',
                    'smj-NO',
                    'smj-SE',
                    'smn-FI',
                    'sms-FI',
                    'sq-AL',
                    'sr-Cyrl-BA',
                    'sr-Cyrl-CS',
                    'sr-Cyrl-ME',
                    'sr-Cyrl-RS',
                    'sr-Latn-BA',
                    'sr-Latn-CS',
                    'sr-Latn-ME',
                    'sr-Latn-RS',
                    'sv-FI',
                    'sv-SE',
                    'sw-KE',
                    'syr-SY',
                    'ta-IN',
                    'te-IN',
                    'tg-Cyrl-TJ',
                    'th-TH',
                    'tk-TM',
                    'tn-ZA',
                    'tr-TR',
                    'tt-RU',
                    'tzm-Latn-DZ',
                    'ug-CN',
                    'uk-UA',
                    'ur-PK',
                    'uz-Cyrl-UZ',
                    'uz-Latn-UZ',
                    'vi-VN',
                    'wo-SN',
                    'xh-ZA',
                    'yo-NG',
                    'zh-CN',
                    'zh-HK',
                    'zh-MO',
                    'zh-SG',
                    'zh-TW',
                    'zu-ZA'];
  
    for(var i=0; i< locales.length;i++) {
        if(locales[i].includes(countryCode, 0))
            return locales[i];
    }
  }
  
async function detectLanguage(text) {
    // dat fiind un text, recunoaste limba in care este scris si returneaza locale-ul tarii (ex ro-RO)
    // foloseste API-ul de text Analytics cu care se comunica folosind `client`
      const languageInputArray = [
        text
      ];
  
      console.log(languageInputArray);
      const languageResult = await textAnalyticsClient.detectLanguage(languageInputArray);
  
      return getLocale(languageResult[0].primaryLanguage.iso6391Name);
    }
  
  
module.exports.uploadFile = async function uploadFile(email, fileName, data, isArrayBuffer=false, callback) {
      // uploadeaza pe cloud un fisier al carui continut este data
        console.log(email);
        const blobName = fileName;
        const AZURE_STORAGE_CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=cartiaudio;AccountKey=21gn/wjLgHjeZGy9yjtORHeFhqLDn+VhvFPrmMkfZgSvIjqBQpL5iaxd0dFQ3Hr7JWpVC/wsX4dPd7xyAjM75Q==;EndpointSuffix=core.windows.net";
        const blobServiceClient = await BlobServiceClient.fromConnectionString(AZURE_STORAGE_CONNECTION_STRING);
        const containerName = email.replace('@', '').replace('.', '');
        const containerClient = await blobServiceClient.getContainerClient(containerName);
        // Get a block blob client
        const blockBlobClient = containerClient.getBlockBlobClient(blobName);
        console.log('\nUploading to Azure storage as blob:\n\t', blobName);
        var dataLength;
        if (isArrayBuffer) {
          dataLength = data.byteLength;
        }
        else {
          dataLength = data.length;
    }
    
    const uploadBlobResponse = await blockBlobClient.upload(data, dataLength);
    
    console.log("Blob was uploaded successfully. requestId: ", uploadBlobResponse.requestId);
    callback();
  }
  
module.exports.textToSpeech = async function textToSpeech(text, username, filename, callback) {
    // converteste un text intr-un fisier audio, pe care il stocheaza dupa in cloud
      var subscriptionKey = "5bc0758a7b3143149e153ee2076cde8e";
      var serviceRegion = "francecentral"; // e.g., "westus"
      var audioConfig = speechSdk.AudioConfig.fromStreamOutput(speechSdk.AudioOutputStream.createPullStream());
      var speechConfig = speechSdk.SpeechConfig.fromSubscription(subscriptionKey, serviceRegion);
      speechConfig.speechSynthesisLanguage = await detectLanguage(text);
  
      var synthesizer = new speechSdk.SpeechSynthesizer(speechConfig, audioConfig);
  
      synthesizer.speakTextAsync(text,
        async function (result) {
  
      if (result.reason === speechSdk.ResultReason.SynthesizingAudioCompleted) {
        console.log("synthesis finished.");
        // continutul fisierului audio este trimis functie de uploadare de fisier pe cloud
        module.exports.uploadFile(username, filename, result.privAudioData, true, function() {
          callback();
        });
  
      } else {
        console.error("Speech synthesis canceled, " + result.errorDetails +
            "\nDid you update the subscription info?");
      }
      synthesizer.close();
      synthesizer = undefined;
      },
        function (err) {
      console.trace("err - " + err);
      synthesizer.close();
      synthesizer = undefined;
    
      });
      console.log("Now synthesizing to: " + filename);

     
      
  }
  
module.exports.generateSASurl = function generateSASurl(userName, blobName, type="mp3") {
    // creaza un link prin care un fisier poate fi accesat pentru o perioada de timp stabilit
    // apeleaza API-ul de blob storage
    
      var containerName = userName.replace('@', '').replace('.', '');
      const storageAccount = "cartiaudio";
      var storageKey = "21gn/wjLgHjeZGy9yjtORHeFhqLDn+VhvFPrmMkfZgSvIjqBQpL5iaxd0dFQ3Hr7JWpVC/wsX4dPd7xyAjM75Q==";
      var baseUrl = `https://${storageAccount}.blob.core.windows.net/${containerName}/${blobName}?`;
  
      var sharedKeyCredential = new blobStorage.StorageSharedKeyCredential(storageAccount, storageKey);
      const blobSAS = blobStorage.generateBlobSASQueryParameters({
          containerName : containerName, // Required
          blobName : blobName, // Required
          permissions: blobStorage.BlobSASPermissions.parse("r"), // Required
          startsOn: new Date(), // Required
          expiresOn: new Date(new Date().valueOf() + 60*5*1000), // Optional. Date type - 5 minute
          protocol: blobStorage.SASProtocol.Https, // Optional
          //cacheControl: "cache-control-override", // Optional
          //contentDisposition: "content-disposition-override", // Optional
          //contentEncoding: "content-encoding-override", // Optional
          //contentLanguage: "content-language-override", // Optional
          contentType: (type == "mp3") ? "audio/mpeg" : "application/pdf", // Optional
          //ipRange: { start: "0.0.0.0", end: "255.255.255.255" }, // Optional
          
          //version: "2016-05-31" // Optional
        },
        sharedKeyCredential // StorageSharedKeyCredential - `new StorageSharedKeyCredential(account, accountKey)`
      ).toString();
  
    return baseUrl+blobSAS;
  }

module.exports.spellCheck = function spellCheck(text, callback) {
    let host = 'spellcheckercc.cognitiveservices.azure.com';
    let path = '/bing/v7.0/spellcheck';
    let spellCheckKey = '0a48b1541c8f45549e84dfd06645dd1f';
    let mkt = "en-US";
    let mode = "proof";
    let newtext = text;
    let query_string = "?mkt=" + mkt + "&mode=" + mode;
    let request_params = {
        method : 'POST',
        hostname : host,
        path : path + query_string,
        headers : {
        'Content-Type' : 'application/x-www-form-urlencoded',
        'Content-Length' : text.length + 5,
           'Ocp-Apim-Subscription-Key' : spellCheckKey,
        }
     };
     let response_handler = function (response) {
        let body = '';
        response.on ('data', function (d) {
            body += d;
        });
        response.on ('end', function () {
            let body_ = JSON.parse (body);
            let tokens = body_.flaggedTokens;
            for(var i = 0; i < tokens.length; i++) {
                newtext = newtext.replace(tokens[i].token, tokens[i].suggestions[0].suggestion);
                //console.log(tokens[i].suggestions);
            }
            //console.log (body_);
            //console.log(newtext);
            callback(newtext)
        });
        response.on ('error', function (e) {
            //console.log ('Error: ' + e.message);
        });
    };
    let req = https.request (request_params, response_handler);
    req.write ("text=" + text);
    req.end ();

    
}