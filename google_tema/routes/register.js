var express = require('express');
var path = require('path');
var Firestore = require('@google-cloud/firestore');
var router = express.Router();
var crypto = require('crypto');

const db = new Firestore();
var usersDB = db.collection('users');

/* GET */
router.get('/', (req, res) => {
  if (req.session.user && req.cookies.user_sid) {
    res.redirect('/dashboard');
  } else
    res.sendFile(path.join(__dirname, '../public', 'signup.html'));
});

/* POST */
router.post('/', (req, res) => {
  usersDB.doc(req.body.email).get()
    .then(doc => {
      if(!doc.exists) {
        req.session.user = {username : req.body.email, password: req.body.password};
        usersDB.doc(req.body.email).set({password: crypto.Hash('sha256').update(req.body.password).digest('hex'), books: []})
          .then(rez => {
            res.redirect('/dashboard');
          });

      } else {
        res.redirect('/login');
      }
    })
    .catch(error => {
      console.log(error);
      res.redirect('/login');
    });
});

module.exports = router;
