var express = require('express');
var router = express.Router();
var upload = require('express-fileupload');
var session = require('express-session');
var Firestore = require('@google-cloud/firestore');
let admin = require('firebase-admin');
const db = new Firestore();
// Imports the Google Cloud client library
const {Translate} = require('@google-cloud/translate').v2;

// Creates a client
const translate = new Translate();




// Imports the Google Cloud client libraries
const vision = require('@google-cloud/vision').v1;

// Creates a client
const vis_client = new vision.ImageAnnotatorClient();


// Imports the Google Cloud client library
const {Storage} = require('@google-cloud/storage');

// Creates a client
const storage = new Storage();
// Creates a client from a Google service account key.
// const storage = new Storage({keyFilename: "key.json"});

/**
 * TODO(developer): Uncomment these variables before running the sample.
 */
// const bucketName = 'bucket-name';



// Imports the Google Cloud client library
const textToSpeech = require('@google-cloud/text-to-speech');

// Import other required libraries
const fs = require('fs');
const util = require('util');
// Creates a client
const client = new textToSpeech.TextToSpeechClient();



router.use(upload({
  useTempFiles : true,
  tempFileDir : '/tmp/'
}));

function userAddBook(username, bookName, author) {
  let user = db.collection('users').doc(username);

// Atomically add a new region to the "regions" array field.
  user.update({
    books: admin.firestore.FieldValue.arrayUnion({title: bookName, author: author})
});
}

async function detectLanguage(text) {
  let [detections] = await translate.detect(text);
  detections = Array.isArray(detections) ? detections : [detections];

  return detections[0].language;
}

async function getVoiceCode(langCode) {
  // returneaza codul limbii daca exista ca voce, altfel returneaza ca standard en-US
  const textToSpeech = require('@google-cloud/text-to-speech');

  const client = new textToSpeech.TextToSpeechClient();

  const [result] = await client.listVoices({});
  const voices = result.voices;
  
  var found = false;
  for(let i = 0; i < voices.length; i++) {
      for(let j = 0; j < voices[i].languageCodes.length; j++) {
          if(voices[i].languageCodes[j].indexOf(langCode) == 0)
          return langCode;
      }
  }
  
  return "en-US";
}

async function uploadFile(user, file, book_title) {
  // Uploads a local file to the bucket

  await storage.bucket('my_bucketone').upload(file.tempFilePath, {
    // Support for HTTP requests made with `Accept-Encoding: gzip`
    //gzip: true,
    destination: `${user}/${book_title}.pdf`,
    // By setting the option `destination`, you can change the name of the
    // object you are uploading to a bucket.
    metadata: {
      contentType: file.mimetype,
      // Enable long-lived HTTP caching headers
      // Use only if the contents of the file will never change
      // (If the contents will change, use cacheControl: 'no-cache')
      //cacheControl: 'public, max-age=31536000',
    },
  });

  

  console.log(`${book_title} uploaded to my_bucketone.`);
  // Construct the request

  var my_bucket = storage.bucket('my_bucketone');
  if (file.mimetype == 'application/pdf') {
    const gcsSourceUri = `gs://my_bucketone/${user}/${book_title}.pdf`;
    const gcsDestinationUri = `gs://my_bucketone/${user}/${book_title}`;

    const inputConfig = {
      // Supported mime_types are: 'application/pdf' and 'image/tiff'
      mimeType: 'application/pdf',
      gcsSource: {
        uri: gcsSourceUri,
      },
    };
    const outputConfig = {
      gcsDestination: {
        uri: gcsDestinationUri,
      
      },
    };
    const features = [{type: 'DOCUMENT_TEXT_DETECTION'}];
    const request = {
      requests: [
        {
          inputConfig: inputConfig,
          features: features,
          outputConfig: outputConfig,
        },
      ],
    };

    const [operation] = await vis_client.asyncBatchAnnotateFiles(request);
    const [filesResponse] = await operation.promise();
    const destinationUri = filesResponse.responses[0].outputConfig.gcsDestination.uri;
    console.log('Json saved to: ' + destinationUri);

    my_bucket.getFiles({
      prefix : `${user}/${book_title}output`,
    }).then(function(files_array)
    {
      var my_file = files_array[0][0];
      my_file.download().then(function(data) {
        json_obj = JSON.parse(data[0].toString('utf-8'));
        //console.log(json_obj["responses"][0]["fullTextAnnotation"]);
        text = String();
        for (var page_info in json_obj["responses"]) {
          text = text + json_obj["responses"][page_info]["fullTextAnnotation"]["text"];
        }
        
        fs.writeFile('my_text', text);
        detectLanguage(text).then(rez => {
          getVoiceCode(rez).then(async function(cod) {
            const request = {
              input: {text: text},
              // Select the language and SSML voice gender (optional)
              voice: {languageCode: cod, ssmlGender: 'NEUTRAL'},
              // select the type of audio encoding
              audioConfig: {audioEncoding: 'MP3'},
            };
          
            // Performs the text-to-speech request
            const [response] = await client.synthesizeSpeech(request);[]
          
            const options = { // construct the file to write
              metadata: {
                contentType: 'audio/mpeg',
                metadata: {
                  source: 'Google Text-to-Speech'
                }
              }
            };
            var myBucket = storage.bucket('my_bucketone');
            var myAudio = myBucket.file(`${user}/${book_title}.mp3`);
            myAudio.save(response.audioContent, options);
            console.log("audio book saved");
            my_file.delete();
          })
        });
    
      });
    });
    
    
  }

  else {
    var my_file = my_bucket.file('user/output-1-to-2.json');
  
  } 
  
}



/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.session.user && req.cookies.user_sid) {
    console.log('ceva');
    res.render('convert');
  } else {
    console.log('ciuciu');
    res.redirect('/login');
  }
});

router.post('/', function(req, res, next) {
  if (req.session.user && req.cookies.user_sid) {
    console.log('s-a trimis fisierul');
    if (req.files) {
      var my_book = req.files.book_text;
      if (my_book.mimetype == "application/pdf") {
        uploadFile(req.session.user.username, req.files.book_text, req.body.titlu).then(function(){
          userAddBook(req.session.user.username, req.body.titlu, req.body.autor);
          res.redirect('/dashboard');
        });
      }
    }
  } else {
    res.redirect('/login');
  }
});


module.exports = router;