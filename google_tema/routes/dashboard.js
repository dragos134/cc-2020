var express = require('express');
var router = express.Router();
var path = require('path');
const fs = require('fs');
const parse = require('node-html-parser').parse;
const {Storage} = require('@google-cloud/storage');
const storage = new Storage();
const myBucket = storage.bucket('my_bucketone');
var Firestore = require('@google-cloud/firestore');

const db = new Firestore();
var users = db.collection('users');

async function createHtml(carti, username) {
  var htmlToAdd = '';
  for(var i = 0; i < carti.length; i++) {
    let book = carti[i];

    let mp3file = myBucket.file(`${username}/${book.title}.mp3`);
    

    let mp3Url = await mp3file.getSignedUrl({
        action: 'read',
        expires: new Date(Date.now()+1000*60*5).toString()
      });
    htmlToAdd = htmlToAdd + `<div>${book.author}-${book.title}`;
    htmlToAdd = htmlToAdd + `  <a href=${mp3Url} target="_blank">Descarca MP3</a>`;

    let pdffile = myBucket.file(`${username}/${book.title}.pdf`);
      
    let pdfUrl = await pdffile.getSignedUrl({
            action: 'read',
            expires: new Date(Date.now()+1000*60*5).toString()
          });  
    htmlToAdd = htmlToAdd + ` <a href=${pdfUrl} target="_blank">Descarca PDF</a></div></br>`;

      //console.log(url);
  }

  return htmlToAdd;
}
/* GET users listing. */
router.get('/', (req, res) => {
  if (req.session.user && req.cookies.user_sid) {

        var htmlToAdd = '';

        users.doc(req.session.user.username).get()
          .then(user => {
            var htmlToAdd = '';
            console.log(user.data());
            if(user.data().books.length == 0) {
              res.sendFile(path.join(__dirname, '../public', 'dashboard.html'));
            } else {
                  let carti = user.data().books;
                  
                  createHtml(carti, req.session.user.username).then(rez => {
                    console.log(rez);
                    fs.readFile(path.join(__dirname, '../public', 'dashboard.html'), 'utf8', (err,html)=>{
                      if(err){
                          throw err;
                      }
          
                      const root = parse(html);
          
                      const body = root.querySelector('body');
                      //body.set_content('<div id = "asdf"></div>');
                      body.appendChild(rez);
          
                      //console.log(root.toString()); // This you can write back to file!
                      res.set('Content-Type', 'text/html');
                      res.send(root.toString());
                    });
                  });
              }
          })
          .catch(err => {
            console.log('Error getting documents', err);
          });

          
          
       

          
      //res.sendFile(path.join(__dirname, '../public', 'dashboard.html'));
  } else {
      res.redirect('/login');
  }
});

module.exports = router;
