var express = require('express');
var path = require('path');
var router = express.Router();
var Firestore = require('@google-cloud/firestore');
var crypto = require('crypto');


const db = new Firestore();
var usersDB = db.collection('users');

/* GET users listing. */
router.get('/', (req, res) => {
  if (req.session.user && req.cookies.user_sid)
    res.redirect('/dashboard');
  else
    res.sendFile(path.join(__dirname, '../public', 'login.html'));
});

/* POST */
router.post('/', (req, res) => {
  var username = req.body.username,
      password = req.body.password;

    usersDB.doc(username).get()
    .then(doc => {
      if(!doc.exists) {
        res.redirect('/');
      } else {
        if(crypto.Hash('sha256').update(password).digest('hex') != doc.data().password)
          res.redirect('/');
        else {
          req.session.user = {username : username, password: password};
          res.redirect('/dashboard');
        }
      }
    });

 
});

module.exports = router;
