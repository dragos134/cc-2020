// Imports the Google Cloud client library
const {Translate} = require('@google-cloud/translate').v2;

// Creates a client
const translate = new Translate();

/**
 * TODO(developer): Uncomment the following line before running the sample.
 */
// const text = 'The text for which to detect language, e.g. Hello, world!';

// Detects the language. "text" can be a string for detecting the language of
// a single piece of text, or an array of strings for detecting the languages
// of multiple texts.
async function detectLanguage(text) {
  let [detections] = await translate.detect(text);
  detections = Array.isArray(detections) ? detections : [detections];

  return detections[0].language;
}

detectLanguage("andi incearca pe un text scurt sa gaseasca limba.")
    .then(rez => {
        console.log(rez);
    })

// Imports the Google Cloud client library
const textToSpeech = require('@google-cloud/text-to-speech');

async function getVoiceCode(langCode) {
    // returneaza codul limbii daca exista ca voce, altfel returneaza ca standard en-US
    const textToSpeech = require('@google-cloud/text-to-speech');
  
    const client = new textToSpeech.TextToSpeechClient();
  
    const [result] = await client.listVoices({});
    const voices = result.voices;
    
    var found = false;
    for(let i = 0; i < voices.length; i++) {
        for(let j = 0; j < voices[i].languageCodes.length; j++) {
            if(voices[i].languageCodes[j].indexOf(langCode) == 0)
            return langCode;
        }
    }
    
    return "en-US";
}

getVoiceCode('ja')
    .then(rez => {
        console.log(rez);
    });
