'use strict';
var express = require('express');
var router = express.Router();
var upload = require('express-fileupload');
var session = require('express-session');
var assert = require("assert").strict;
const { BlobServiceClient } = require('@azure/storage-blob');
const fs = require('fs');
const path = require("path");
const createReadStream = require('fs').createReadStream
const sleep = require('util').promisify(setTimeout);
const ComputerVisionClient = require('@azure/cognitiveservices-computervision').ComputerVisionClient;
const ApiKeyCredentials = require('@azure/ms-rest-js').ApiKeyCredentials;
const request = require('request');
var blobStorage = require('@azure/storage-blob');
const { TextAnalyticsClient, AzureKeyCredential } = require("@azure/ai-text-analytics");
var sdk = require("microsoft-cognitiveservices-speech-sdk");

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
// url-ul catre serverul tau mongo
var url = 'mongodb://andi-mongo-cc:Vc1QFpJ7PyW78Ue32DCiw6VNCVTRbUqLJUimjouwwvhykF9rIWVFESAoLxBl2Ew6UIWphc6c3jy7OHlhnkdIPA%3D%3D@andi-mongo-cc.mongo.cosmos.azure.com:10255/?ssl=true&appName=@andi-mongo-cc@/?ssl=true';
var collectionName = 'users';
var dbName = 'tema3_db';

var endpoint = "https://francecentral.api.cognitive.microsoft.com/text/analytics/v2.1/languages";
var languageKey = "754f58a28b804d1d962f5117e7347bdb";
var languageEndpoint = "https://text-analytics-cc.cognitiveservices.azure.com/text/analytics/v2.1/languages";
const textAnalyticsClient = new TextAnalyticsClient(languageEndpoint,  new AzureKeyCredential(languageKey));

router.use(upload({
}));

function generateSASurl(userName, blobName) {
  var containerName = userName.replace('@', '').replace('.', '');
  const storageAccount = "cartiaudio";
  var storageKey = "21gn/wjLgHjeZGy9yjtORHeFhqLDn+VhvFPrmMkfZgSvIjqBQpL5iaxd0dFQ3Hr7JWpVC/wsX4dPd7xyAjM75Q==";
  var baseUrl = `https://${storageAccount}.blob.core.windows.net/${containerName}/${blobName}?`;

  var sharedKeyCredential = new blobStorage.StorageSharedKeyCredential(storageAccount, storageKey);

  const blobSAS = blobStorage.generateBlobSASQueryParameters({
      containerName : containerName, // Required
      blobName : blobName, // Required
      permissions: blobStorage.BlobSASPermissions.parse("r"), // Required
      startsOn: new Date(), // Required
      expiresOn: new Date(new Date().valueOf() + 60*5*1000), // Optional. Date type
      protocol: blobStorage.SASProtocol.Https, // Optional
      //cacheControl: "cache-control-override", // Optional
      //contentDisposition: "content-disposition-override", // Optional
      //contentEncoding: "content-encoding-override", // Optional
      //contentLanguage: "content-language-override", // Optional
      //contentType: "content-type-override", // Optional
      //ipRange: { start: "0.0.0.0", end: "255.255.255.255" }, // Optional
      
      //version: "2016-05-31" // Optional
    },
    sharedKeyCredential // StorageSharedKeyCredential - `new StorageSharedKeyCredential(account, accountKey)`
  ).toString();

  return baseUrl+blobSAS;
}

function userAddBook(username, bookName, author) {

  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    var db = client.db(dbName);
    var userDB = db.collection(collectionName);
    userDB.update(
      {"email" : username},
      {$push : {"books" : {title: bookName, author: author}}}
    );
  });

}


function getLocale(countryCode) {
  // list-of-all-locales-and-their-short-codes
  var locales = ['af-ZA',
                  'am-ET',
                  'ar-EG',
                  'arn-CL',
                  'as-IN',
                  'az-Cyrl-AZ',
                  'az-Latn-AZ',
                  'ba-RU',
                  'be-BY',
                  'bg-BG',
                  'bn-BD',
                  'bn-IN',
                  'bo-CN',
                  'br-FR',
                  'bs-Cyrl-BA',
                  'bs-Latn-BA',
                  'ca-ES',
                  'co-FR',
                  'cs-CZ',
                  'cy-GB',
                  'da-DK',
                  'de-DE',
                  'dsb-DE',
                  'dv-MV',
                  'el-GR',
                  'en-US',
                  'es-ES',
                  'et-EE',
                  'eu-ES',
                  'fa-IR',
                  'fi-FI',
                  'fil-PH',
                  'fo-FO',
                  'fr-FR',
                  'fy-NL',
                  'ga-IE',
                  'gd-GB',
                  'gl-ES',
                  'gsw-FR',
                  'gu-IN',
                  'ha-Latn-NG',
                  'he-IL',
                  'hi-IN',
                  'hr-HR',
                  'hsb-DE',
                  'hu-HU',
                  'hy-AM',
                  'id-ID',
                  'ig-NG',
                  'ii-CN',
                  'is-IS',
                  'it-IT',
                  'iu-Cans-CA',
                  'iu-Latn-CA',
                  'ja-JP',
                  'ka-GE',
                  'kk-KZ',
                  'kl-GL',
                  'km-KH',
                  'kn-IN',
                  'kok-IN',
                  'ko-KR',
                  'ky-KG',
                  'lb-LU',
                  'lo-LA',
                  'lt-LT',
                  'lv-LV',
                  'mi-NZ',
                  'mk-MK',
                  'ml-IN',
                  'mn-MN',
                  'mn-Mong-CN',
                  'moh-CA',
                  'mr-IN',
                  'ms-BN',
                  'ms-MY',
                  'mt-MT',
                  'nb-NO',
                  'ne-NP',
                  'nl-BE',
                  'nl-NL',
                  'nn-NO',
                  'nso-ZA',
                  'oc-FR',
                  'or-IN',
                  'pa-IN',
                  'pl-PL',
                  'prs-AF',
                  'ps-AF',
                  'pt-BR',
                  'pt-PT',
                  'qut-GT',
                  'quz-BO',
                  'quz-EC',
                  'quz-PE',
                  'rm-CH',
                  'ro-RO',
                  'ru-RU',
                  'rw-RW',
                  'sah-RU',
                  'sa-IN',
                  'se-FI',
                  'se-SE',
                  'si-LK',
                  'sk-SK',
                  'sl-SI',
                  'sma-NO',
                  'sma-SE',
                  'smj-NO',
                  'smj-SE',
                  'smn-FI',
                  'sms-FI',
                  'sq-AL',
                  'sr-Cyrl-BA',
                  'sr-Cyrl-CS',
                  'sr-Cyrl-ME',
                  'sr-Cyrl-RS',
                  'sr-Latn-BA',
                  'sr-Latn-CS',
                  'sr-Latn-ME',
                  'sr-Latn-RS',
                  'sv-FI',
                  'sv-SE',
                  'sw-KE',
                  'syr-SY',
                  'ta-IN',
                  'te-IN',
                  'tg-Cyrl-TJ',
                  'th-TH',
                  'tk-TM',
                  'tn-ZA',
                  'tr-TR',
                  'tt-RU',
                  'tzm-Latn-DZ',
                  'ug-CN',
                  'uk-UA',
                  'ur-PK',
                  'uz-Cyrl-UZ',
                  'uz-Latn-UZ',
                  'vi-VN',
                  'wo-SN',
                  'xh-ZA',
                  'yo-NG',
                  'zh-CN',
                  'zh-HK',
                  'zh-MO',
                  'zh-SG',
                  'zh-TW',
                  'zu-ZA'];

  for(var i=0; i< locales.length;i++) {
      if(locales[i].includes(countryCode, 0))
          return locales[i];
  }
}


async function detectLanguage(client, text) {

  const languageInputArray = [
      text
  ];

  const languageResult = await client.detectLanguage(languageInputArray);

  return languageResult[0].primaryLanguage.iso6391Name;

  /* languageResult.forEach(document => {
      console.log(`ID: ${document.id}`);
      console.log(document.primaryLanguage.iso6391Name);
      console.log(`\tPrimary Language ${document.primaryLanguage.name}`)
  }); */
}

async function textToSpeech(text, langCode, username, filename) {
  var subscriptionKey = "5bc0758a7b3143149e153ee2076cde8e";
  var serviceRegion = "francecentral"; // e.g., "westus"
  var audioConfig = sdk.AudioConfig.fromStreamOutput(sdk.AudioOutputStream.createPullStream());
  var speechConfig = sdk.SpeechConfig.fromSubscription(subscriptionKey, serviceRegion);
  speechConfig.speechSynthesisLanguage = langCode;

  var synthesizer = new sdk.SpeechSynthesizer(speechConfig, audioConfig);

  synthesizer.speakTextAsync(text,
      function (result) {

    if (result.reason === sdk.ResultReason.SynthesizingAudioCompleted) {
      console.log("synthesis finished.");
      uploadFile(username, filename, result.privAudioData, true);

    } else {
      console.error("Speech synthesis canceled, " + result.errorDetails +
          "\nDid you update the subscription info?");
    }
    synthesizer.close();
    synthesizer = undefined;
  },
      function (err) {
    console.trace("err - " + err);
    synthesizer.close();
    synthesizer = undefined;
  });
  console.log("Now synthesizing to: " + filename);
}


function detecto(imgUrl, username, title) {
  let subscriptionKey = "ccd0eb9d3a3e46f385585c7d0e0a24f1";
  let endpoint = "https://computer-vision-ccc.cognitiveservices.azure.com/";
  if (!subscriptionKey) { throw new Error('Set your environment variables for your subscription key and endpoint.'); }

  var uriBase = endpoint + 'vision/v2.1/ocr';

  const imageUrl = imgUrl;

  // Request parameters.
  const params = {
      'language': 'unk',
      'detectOrientation': 'true',
  };

  const options = {
      uri: uriBase,
      qs: params,
      body: `{"url": "${imageUrl}"}`,
      headers: {
          'Content-Type': 'application/json',
          'Ocp-Apim-Subscription-Key' : subscriptionKey
      }
  };

  request.post(options, (error, response, body) => {
    if (error) {
      console.log('Error: ', error);
      return;
    }
    console.log(body);
    var finalResult = "";
    let jsonResponse = JSON.parse(body);
    for (var region in jsonResponse["regions"]) {
      for (var line in jsonResponse["regions"][region]["lines"]) {
        for (var word in jsonResponse["regions"][region]["lines"][line]["words"]) {
          finalResult = finalResult + ' ' + jsonResponse["regions"][region]["lines"][line]["words"][word]["text"];
        }
        finalResult = finalResult + '\n';
      }
    }
    console.log("rezultatul final este: " + finalResult);
    detectLanguage(textAnalyticsClient, finalResult).then(function(res) {

      console.log(getLocale(res));
      textToSpeech(finalResult, getLocale(res), username, title + '.mp3');
    });
  });
}

async function uploadFile(email, fileName, data, isArrayBuffer=false) {
  console.log(email);
  const blobName = fileName;
  const AZURE_STORAGE_CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=cartiaudio;AccountKey=21gn/wjLgHjeZGy9yjtORHeFhqLDn+VhvFPrmMkfZgSvIjqBQpL5iaxd0dFQ3Hr7JWpVC/wsX4dPd7xyAjM75Q==;EndpointSuffix=core.windows.net";
  const blobServiceClient = await BlobServiceClient.fromConnectionString(AZURE_STORAGE_CONNECTION_STRING);
  const containerName = email.replace('@', '').replace('.', '');
  const containerClient = await blobServiceClient.getContainerClient(containerName);
  // Get a block blob client
  const blockBlobClient = containerClient.getBlockBlobClient(blobName);
  console.log('\nUploading to Azure storage as blob:\n\t', blobName);
  var dataLength;
  if (isArrayBuffer) {
    dataLength = data.byteLength;
  }
  else {
    dataLength = data.length;
  }

  const uploadBlobResponse = await blockBlobClient.upload(data, dataLength);
  
  console.log("Blob was uploaded successfully. requestId: ", uploadBlobResponse.requestId);
}




router.get('/', function(req, res, next) {
  if (req.session.user && req.cookies.user_sid) {
    res.render('convert');

  } else {
    res.redirect('/login');
  }
});

router.post('/', function(req, res, next) {
  if (req.session.user && req.cookies.user_sid) {
    console.log('s-a trimis fisierul');
    if (req.files) {
      var my_book = req.files.book_text;
      console.log(my_book.data);
      console.log(req.body.titlu);
      uploadFile(req.session.user.username, req.body.titlu, my_book.data, false).then(function(){
        var imgUrl = generateSASurl(req.session.user.username, req.body.titlu);
        console.log(imgUrl, req.session.user.username, req.body.titlu);
        detecto(imgUrl, req.session.user.username, req.body.titlu);
        userAddBook(req.session.user.username, req.body.titlu, req.body.autor);
        res.redirect('/dashboard');
      });
    }
  } else {

    res.redirect('/login');
  }
});


module.exports = router;