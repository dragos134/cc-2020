var express = require('express');
var router = express.Router();
var path = require('path');
const fs = require('fs');
const parse = require('node-html-parser').parse;

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
// url-ul catre serverul tau mongo
var url = 'mongodb://andi-mongo-cc:Vc1QFpJ7PyW78Ue32DCiw6VNCVTRbUqLJUimjouwwvhykF9rIWVFESAoLxBl2Ew6UIWphc6c3jy7OHlhnkdIPA%3D%3D@andi-mongo-cc.mongo.cosmos.azure.com:10255/?ssl=true&appName=@andi-mongo-cc@/?ssl=true';
var collectionName = 'users';
var dbName = 'tema3_db';

var blobStorage = require('@azure/storage-blob');

const storageAccount = "cartiaudio";
var storageKey = "21gn/wjLgHjeZGy9yjtORHeFhqLDn+VhvFPrmMkfZgSvIjqBQpL5iaxd0dFQ3Hr7JWpVC/wsX4dPd7xyAjM75Q==";


function generateSASurl(containerName, blobName) {
  var baseUrl = `https://${storageAccount}.blob.core.windows.net/${containerName}/${blobName}?`;

  var sharedKeyCredential = new blobStorage.StorageSharedKeyCredential(storageAccount, storageKey);

  const blobSAS = blobStorage.generateBlobSASQueryParameters({
      containerName : containerName, // Required
      blobName : blobName, // Required
      permissions: blobStorage.BlobSASPermissions.parse("r"), // Required
      startsOn: new Date(), // Required
      expiresOn: new Date(new Date().valueOf() + 60*5*1000), // Optional. Date type
      protocol: blobStorage.SASProtocol.Https, // Optional
      //cacheControl: "cache-control-override", // Optional
      //contentDisposition: "content-disposition-override", // Optional
      //contentEncoding: "content-encoding-override", // Optional
      //contentLanguage: "content-language-override", // Optional
      //contentType: "content-type-override", // Optional
      //ipRange: { start: "0.0.0.0", end: "255.255.255.255" }, // Optional
      
      //version: "2016-05-31" // Optional
    },
    sharedKeyCredential // StorageSharedKeyCredential - `new StorageSharedKeyCredential(account, accountKey)`
  ).toString();

  return baseUrl+blobSAS;
}

async function createHtml(carti, username) {
  var htmlToAdd = '';
  for(var i = 0; i < carti.length; i++) {
    let book = carti[i];

    //let mp3file = myBucket.file(`${username}/${book.title}.mp3`);
    

    let mp3Url = generateSASurl(username, `${book.title}.mp3`);
    /* await mp3file.getSignedUrl({
        action: 'read',
        expires: new Date(Date.now()+1000*60*5).toString()
      }); */
    htmlToAdd = htmlToAdd + `<div>${book.author}-${book.title}`;
    htmlToAdd = htmlToAdd + `  <a href=${mp3Url} target="_blank">Descarca MP3</a>`;

    //let pdffile = myBucket.file(`${username}/${book.title}.pdf`);
      
    let pdfUrl = generateSASurl(username, `${book.title}`);
    /* await pdffile.getSignedUrl({
            action: 'read',
            expires: new Date(Date.now()+1000*60*5).toString()
          }); */  
    htmlToAdd = htmlToAdd + ` <a href=${pdfUrl} target="_blank">Descarca PDF</a></div></br>`;

      //console.log(url);
  }

  return htmlToAdd;
}
/* GET users listing. */
router.get('/', (req, res) => {
  if (req.session.user && req.cookies.user_sid) {

        MongoClient.connect(url, function(err, client) {
          assert.equal(null, err);
          var db = client.db(dbName);
          var usersDB = db.collection(collectionName);
          usersDB.findOne({"email" : req.session.user.username}).then(function (user) {
            if(user.books.length == 0) {
              res.sendFile(path.join(__dirname, '../public', 'dashboard.html'));
            } else {
                let carti = user.books;
                createHtml(carti, req.session.user.username.replace('@', '').replace('.', '')).then(rez => {
                  //console.log(rez);
                  fs.readFile(path.join(__dirname, '../public', 'dashboard.html'), 'utf8', (err,html)=>{
                    if(err){
                        throw err;
                    }
        
                    const root = parse(html);
        
                    const body = root.querySelector('body');
                    //body.set_content('<div id = "asdf"></div>');
                    body.appendChild(rez);
        
                    //console.log(root.toString()); // This you can write back to file!
                    res.set('Content-Type', 'text/html');
                    res.send(root.toString());
                  });
                });
            
            }
        });
      });

        /* users.doc(req.session.user.username).get()
          .then(user => {
            var htmlToAdd = '';
            console.log(user.data());
            if(user.data().books.length == 0) {
              res.sendFile(path.join(__dirname, '../public', 'dashboard.html'));
            } else {
                  let carti = user.data().books;
                  
                  createHtml(carti, req.session.user.username).then(rez => {
                    console.log(rez);
                    fs.readFile(path.join(__dirname, '../public', 'dashboard.html'), 'utf8', (err,html)=>{
                      if(err){
                          throw err;
                      }
          
                      const root = parse(html);
          
                      const body = root.querySelector('body');
                      //body.set_content('<div id = "asdf"></div>');
                      body.appendChild(rez);
          
                      //console.log(root.toString()); // This you can write back to file!
                      res.set('Content-Type', 'text/html');
                      res.send(root.toString());
                    });
                  });
              }
          })
          .catch(err => {
            console.log('Error getting documents', err);
          }); */

          
          
       

          
      //res.sendFile(path.join(__dirname, '../public', 'dashboard.html'));
  } else {
      res.redirect('/login');
  }
});

module.exports = router;
