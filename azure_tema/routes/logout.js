var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid');
        res.redirect('/login');
    } else {
        res.redirect('/login');
    }
});

module.exports = router;
