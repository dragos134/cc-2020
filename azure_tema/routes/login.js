var express = require('express');
var path = require('path');
var router = express.Router();
var crypto = require('crypto');


var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
// url-ul catre serverul tau mongo
var url = 'mongodb://andi-mongo-cc:Vc1QFpJ7PyW78Ue32DCiw6VNCVTRbUqLJUimjouwwvhykF9rIWVFESAoLxBl2Ew6UIWphc6c3jy7OHlhnkdIPA%3D%3D@andi-mongo-cc.mongo.cosmos.azure.com:10255/?ssl=true&appName=@andi-mongo-cc@/?ssl=true';
var collectionName = 'users';
var dbName = 'tema3_db';

/* GET users listing. */
router.get('/', (req, res) => {
  if (req.session.user && req.cookies.user_sid)
    res.redirect('/dashboard');
  else
    res.sendFile(path.join(__dirname, '../public', 'login.html'));
});

/* POST */
router.post('/', (req, res) => {
  var username = req.body.username,
      password = req.body.password;
  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    var db = client.db(dbName);
    var usersDB = db.collection(collectionName);
    usersDB.findOne({"email" : username}).then(function (user) {
        if(user == null) {
          res.redirect('/');
        } else {
          if(crypto.Hash('sha256').update(password).digest('hex') != user.password)
            res.redirect('/');
          else {
            req.session.user = {username : username, password: password};
            res.redirect('/dashboard');
          }
        }
    });
  });

   /*  usersDB.doc(username).get()
    .then(doc => {
      if(!doc.exists) {
        res.redirect('/');
      } else {
        if(crypto.Hash('sha256').update(password).digest('hex') != doc.data().password)
          res.redirect('/');
        else {
          req.session.user = {username : username, password: password};
          res.redirect('/dashboard');
        }
      }
    }); */

 
});

module.exports = router;
