var express = require('express');
var path = require('path');
var router = express.Router();
var crypto = require('crypto');

const { BlobServiceClient } = require('@azure/storage-blob');

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
// url-ul catre serverul tau mongo
var url = 'mongodb://andi-mongo-cc:Vc1QFpJ7PyW78Ue32DCiw6VNCVTRbUqLJUimjouwwvhykF9rIWVFESAoLxBl2Ew6UIWphc6c3jy7OHlhnkdIPA%3D%3D@andi-mongo-cc.mongo.cosmos.azure.com:10255/?ssl=true&appName=@andi-mongo-cc@/?ssl=true';
var collectionName = 'users';
var dbName = 'tema3_db';

const AZURE_STORAGE_CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=cartiaudio;AccountKey=21gn/wjLgHjeZGy9yjtORHeFhqLDn+VhvFPrmMkfZgSvIjqBQpL5iaxd0dFQ3Hr7JWpVC/wsX4dPd7xyAjM75Q==;EndpointSuffix=core.windows.net";

async function createContainer(email) {
  console.log('start create container');
  // Create the BlobServiceClient object which will be used to create a container client
  const blobServiceClient = await BlobServiceClient.fromConnectionString(AZURE_STORAGE_CONNECTION_STRING);

  
  // Create a unique name for the container
  const containerName = email.replace('@', '').replace('.', '');

  

  console.log('\nCreating container...');
  console.log('\t', containerName);

  // Get a reference to a container
  const containerClient = await blobServiceClient.getContainerClient(containerName);

  // Create the container
  const createContainerResponse = await containerClient.create();
  console.log("Container was created successfully. requestId: ", createContainerResponse.requestId);
}




/* GET */
router.get('/', (req, res) => {
  if (req.session.user && req.cookies.user_sid) {
    res.redirect('/dashboard');
  } else
    res.sendFile(path.join(__dirname, '../public', 'signup.html'));
});

/* POST */
router.post('/', (req, res) => {
  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    var db = client.db(dbName);
    var usersDB = db.collection(collectionName);
    usersDB.findOne({"email" : req.body.email}).then(function (user) {
        if(user == null) {
          createContainer(req.body.email);
          req.session.user = {username : req.body.email, password: req.body.password};
          usersDB.insertOne({
            "email" : req.body.email,
            "password" : crypto.Hash('sha256').update(req.body.password).digest('hex'),
            "books" : []
          }).then(function () { res.redirect('/dashboard')});
        } else {
          res.redirect('/login');
        }
    });
  });
  /* usersDB.doc(req.body.email).get()
    .then(doc => {
      if(!doc.exists) {
        req.session.user = {username : req.body.email, password: req.body.password};
        usersDB.doc(req.body.email).set({password: crypto.Hash('sha256').update(req.body.password).digest('hex'), books: []})
          .then(rez => {
            res.redirect('/dashboard');
          });

      } else {
        res.redirect('/login');
      }
    })
    .catch(error => {
      console.log(error);
      res.redirect('/login');
    }); */
});

module.exports = router;
