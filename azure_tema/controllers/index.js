const Firestore = require('@google-cloud/firestore');
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const crypto = require('crypto');

const db = new Firestore({
  projectId: 'am-cc-2020',
  keyFilename: '/home/andim/Documents/Anu 3/Sem 2/CC/Laboratoare/AM-CC-2020-97caebf0b2bd.json',
});

var users = db.collection('users');
var app = express();

app.set('port', 9000);


// initialize body-parser to parse incoming parameters requests to req.body
app.use(bodyParser.urlencoded({ extended: true }));

// initialize cookie-parser to allow us access the cookies stored in the browser. 
app.use(cookieParser());

// initialize express-session to allow us track the logged-in user across sessions.
app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));


// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
app.use((req, res, next) => {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid');        
    }
    next();
});


// middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
    if (req.session.user && req.cookies.user_sid) {
        res.redirect('/dashboard');
    } else {
        next();
    }    
};


// route for Home-Page
app.get('/', sessionChecker, (req, res) => {
    res.redirect('/login');
});


// route for user signup
app.route('/signup')
    .get(sessionChecker, (req, res) => {
        res.sendFile(__dirname + '/public/signup.html');
    })
    .post((req, res) => {
        users.doc(req.body.email).get()
          .then(doc => {
            if(!doc.exists) {
              users.doc(req.body.email).set({password: crypto.Hash('sha256').update(req.body.password).digest('hex')})
              req.session.user = {username : username, password: password};
              res.redirect('/dashboard');
            } else {
              res.redirect('/signup');
            }
          })
          .catch(error => {
            res.redirect('/signup');
          });
    });


// route for user Login
app.route('/login')
    .get(sessionChecker, (req, res) => {
        res.sendFile(__dirname + '/public/login.html');
    })
    .post((req, res) => {
        var username = req.body.username,
            password = req.body.password;

        users.doc(username).get()
          .then(doc => {
            if(!doc.exists) {
              res.redirect('/login');
            } else {
              if(crypto.Hash('sha256').update(password).digest('hex') != doc.data().password)
                res.redirect('/login');
              else {
                req.session.user = {username : username, password: password};
                res.redirect('/dashboard');
              }
            }
          });

       
    });


// route for user's dashboard
app.get('/dashboard', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
        res.sendFile(__dirname + '/public/dashboard.html');
    } else {
        res.redirect('/login');
    }
});


// route for user logout
app.get('/logout', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid');
        res.redirect('/');
    } else {
        res.redirect('/login');
    }
});


// route for handling 404 requests(unavailable routes)
app.use(function (req, res, next) {
  res.status(404).send("Sorry can't find that!")
});


// start the express server
app.listen(app.get('port'), () => console.log(`App started on port ${app.get('port')}`));

