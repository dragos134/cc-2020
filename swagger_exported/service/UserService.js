
/**
 * Add new porject into database
 * Add new porject into database
 *
 * project Project The new project to be added or updated, if there is no existitg entry with project's id (optional)
 * returns Object
 **/
exports.addproject = function(project) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Delete project by id
 * Delete project by id
 *
 * projectId String Project's id (optional)
 * no response value expected for this operation
 **/
exports.deleteproject = function(projectId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get all projects in certain workspace
 * Get all projects in certain workspace
 *
 * public Boolean If public is true, then returns all public workspace items, else will return only current user's projects (optional)
 * returns List
 **/
exports.getworkspace = function(public) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "firstName" : "Ion",
  "lastName" : "Ionescu",
  "password" : "password",
  "id" : "id",
  "registeredDatetime" : "mm:hh dd-mm-yyyy",
  "email" : "test@yahoo.com"
}, {
  "firstName" : "Ion",
  "lastName" : "Ionescu",
  "password" : "password",
  "id" : "id",
  "registeredDatetime" : "mm:hh dd-mm-yyyy",
  "email" : "test@yahoo.com"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Send image and receive text, the request is performed via azure services
 * Send image and receive text, the request is performed via azure services
 *
 * dataUri String Base64 encoded image (optional)
 * returns Object
 **/
exports.imagetoapi = function(dataUri) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * (Optional) Send paragraph and receive possible continue variants. Azure API is used
 * (Optional) Send paragraph and receive possible continue variants. Azure API is used
 *
 * text String Complete paraghraph to get ideas for continuation (optional)
 * returns List
 **/
exports.paragraphvaraints = function(text) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ "", "" ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get complete project value, including Content field
 * Get complete project value, including Content field
 *
 * projectId String The partially loaded project id, as known from previous /get_workspace response (optional)
 * returns Project
 **/
exports.readproject = function(projectId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "isPublic" : true,
  "createdDatetime" : "mm:hh dd-mm-yyyy",
  "id" : "id",
  "ownerId" : "ownerId",
  "title" : "title",
  "content" : "content"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Set project's visibility
 * Set project's visibility
 *
 * projectId String Project's id (optional)
 * no response value expected for this operation
 **/
exports.setprojectvisibility = function(projectId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Send text and receive mp3 stream. Processing is done via
 * Send text and receive mp3 stream. Processing is done via Azure API
 *
 * text String Text to be transofmed to speech (optional)
 * returns Object
 **/
exports.testtospeech = function(text) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

