
/**
 * Authenticate user
 * Authenticate user
 *
 * data Data_1 Authentication data (optional)
 * returns inline_response_200
 **/
exports.authenticate = function(data) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "id" : 0
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get all projects in certain workspace
 * Get all projects in certain workspace
 *
 * public Boolean If public is true, then returns all public workspace items, else will return only current user's projects (optional)
 * returns List
 **/
exports.getworkspace = function(public) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "firstName" : "Ion",
  "lastName" : "Ionescu",
  "password" : "password",
  "id" : "id",
  "registeredDatetime" : "mm:hh dd-mm-yyyy",
  "email" : "test@yahoo.com"
}, {
  "firstName" : "Ion",
  "lastName" : "Ionescu",
  "password" : "password",
  "id" : "id",
  "registeredDatetime" : "mm:hh dd-mm-yyyy",
  "email" : "test@yahoo.com"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get complete project value, including Content field
 * Get complete project value, including Content field
 *
 * projectId String The partially loaded project id, as known from previous /get_workspace response (optional)
 * returns Project
 **/
exports.readproject = function(projectId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "isPublic" : true,
  "createdDatetime" : "mm:hh dd-mm-yyyy",
  "id" : "id",
  "ownerId" : "ownerId",
  "title" : "title",
  "content" : "content"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Register new user in dababase
 * Adds an item to the system
 *
 * data Data  (optional)
 * no response value expected for this operation
 **/
exports.register = function(data) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

