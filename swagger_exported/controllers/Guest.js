

var utils = require('../utils/writer.js');
var Guest = require('../service/GuestService');

module.exports.authenticate = function authenticate (req, res, next) {
  var data = req.swagger.params['data'].value;
  Guest.authenticate(data)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getworkspace = function getworkspace (req, res, next) {
  var public = req.swagger.params['public'].value;
  Guest.getworkspace(public)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.readproject = function readproject (req, res, next) {
  var projectId = req.swagger.params['projectId'].value;
  Guest.readproject(projectId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.register = function register (req, res, next) {
  var data = req.swagger.params['data'].value;
  Guest.register(data)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
