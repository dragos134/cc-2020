
var utils = require('../utils/writer.js');
var User = require('../service/UserService');

module.exports.addproject = function addproject (req, res, next) {
  var project = req.swagger.params['project'].value;
  User.addproject(project)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteproject = function deleteproject (req, res, next) {
  var projectId = req.swagger.params['projectId'].value;
  User.deleteproject(projectId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getworkspace = function getworkspace (req, res, next) {
  var public = req.swagger.params['public'].value;
  User.getworkspace(public)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.imagetoapi = function imagetoapi (req, res, next) {
  var dataUri = req.swagger.params['dataUri'].value;
  User.imagetoapi(dataUri)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.paragraphvaraints = function paragraphvaraints (req, res, next) {
  var text = req.swagger.params['text'].value;
  User.paragraphvaraints(text)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.readproject = function readproject (req, res, next) {
  var projectId = req.swagger.params['projectId'].value;
  User.readproject(projectId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.setprojectvisibility = function setprojectvisibility (req, res, next) {
  var projectId = req.swagger.params['projectId'].value;
  User.setprojectvisibility(projectId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.testtospeech = function testtospeech (req, res, next) {
  var text = req.swagger.params['text'].value;
  User.testtospeech(text)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
